# Implementing Storage Infrastructure Services
## Zoning

**Verify zoning mode**
`show zone status vsan <vsan>`  
### Configure zone and zoneset
1.  Get the vsan number and pwwn or alias info.  
`show fcns databse`
2.  Configure the zone and add a device-alias to it:  
    ```
    configure
    zone name <zone_name> vsan <vsan>
    member device-alias <alias>
    ```
    Example:  
    **N9K-A(config)# zone name Server-NetApp vsan 1007  
    N9K-A(config)# member device-alias Server**  
3.  Add the zone you've created to a zoneset:  
    ```
    zoneset name <zoneset_name> vsan <vsan>
    member <zone>
    ```
    Example:  
    ***N9K-A(config)# zoneset name Zoneset-Server-NetApp vsan 1007  
    N9K-A(config)# member Server-NetApp***  
4.  Investigate the configured and active zones and zonesets:  
    `show zone vsan <vsan>`  
    `show zone active vsan <vsan>`  
    `show zoneset vsan <vsan>`  
    `show zoneset active <vsan>`  
5.  Check the zoning information on the MDS, n ozoning information should been distributed because in basic mode the full zoning information is distributed on demand, as oposed to the automatic distribution in the enhanced mode.  
6.  Configure the distribution of a full zone set for your VSAN.  
    `zoneset distribute full vsan <vsan>`  
> :bulb: You must explicitly issue the **copy running-config startup-config** command to save the full zone set information to the startup configuration. 
7.  Re-examine the zoning information on the MDS, no zoning information will be distributed untill you activate a zoneset.
8.  Activate the zoneset on the Nexus switch:  
    `zoneset activate name <zoneset_name> vsan <vsan>`  
    Example:  
    **N9K-A(config)# zoneset activate name Zoneset-Server-NetApp vsan 1007**  
*Zoneset activation initiated. check zone status*  
9.  Validate the active zone in your VSAN on the Nexus switch:  
    `show zone active vsan <vsan>`  
    Example:
    ```
    N9K-A# show zone active vsan 1007 
    zone name Server-NetApp vsan 1007* 
     * fcid 0x1c0000 [device-alias Server]
    ```
> :bulb: An asteriks shows that the device is correctly in to the fabric. A missing asterisk may indicate an offline device or an incorrectly configured zone, probably a mistyped pWWN.  
10. Verify that the configured and active zones are displayed on the MDS.  
    ```
    show zone vsan <vsan>
    show zone active vsan <vsan>
    show zoneset vsan <vsan>
    show zoneset active vsan <vsan>
    ```
    Example:
    ```
    MDS# show zone vsan 1007
    zone name Server-NetApp vsan 1007
    device-alias Server  
    ```  
    ```
    MDS# show zone active vsan 1007
    zone name Server-NetApp vsan 1007
    * fcid 0x1c0000 [device-alias Server]
    ```
    ```
    MDS# show zoneset vsan 1007
    zoneset name Zoneset-Server-NetApp vsan 1007
    zone name Server-NetApp vsan 1007
        device-alias Server
    ```
    ```
    MDS# show zoneset active vsan 1007
    zoneset name Zoneset-Server-NetApp vsan 1007
    zone name Server-NetApp vsan 1007
    * fcid 0x1c0000 [device-alias Server]
    ```
### Configure Enhanced Zoning
In production networks, it is recommended that you configure enhanced zoning on both SAN A and SAN B. It is also recommended that you configure zoning in a consistend fashion - for example, use device aliases for all entries on both sides of the SAN. Consistency helps to make documentation clearer and simplifies potential troubleshooting processes.
1.  On MdS, enable enhanced zoning for your VSAN.  
    `zone mode enhanced vsan <vsan>`
    Example: 
    ``` 
    MDS(config)# zone mode enhanced vsan 1028
    WARNING: This command would distribute the zoning database of this switch throughout the fabric. Do you want to continue? (y/n) [n] y
    Set zoning mode command initiated. Check zone status
    ```  
2.  Verify the zone modes on the Nexus and MDS:  
    `show zone status vsan <vsan>`  
3.  Add a new device alias to the fabric. This device alias should be synced with the MDS.  
4.  Add the device alias as a member to a zone:  
    ```
    zone name <zone_name> vsan <vsan>
    member device-alias <alias_name>
    ```
    Example:
    ```
    N9K-A(config)# 
    zone name Server-NetApp vsan 1007
     member device-alias test
    Enhanced zone session has been created. Please 'commit' the changes when done.
    ```
5.  Examine the pending and active zone database:  
    `show zone pending vsan <vsan>`  
    `show zone active vsan <vsan>`
    Example:  
    ```
    N9K-A# show zone pending vsan 1007
    zone name Server-NetApp vsan 1007
      device-alias Server
      device-alias test
    ```
    ```  
    N9K-A# show zone active vsan 1007
    zone name Server-NetApp vsan 1007
    * fcid 0x1c0000 [device-alias Server]
    ```
6.  On the Nexus, activate the zoneset and commit the changes:  
    ```
    zoneset activate name <zoneset_name> vsan <vsan>
    zone commit vsan <vsan>
    ```
    Example:  
    ```
    N9K-A(config)# zoneset activate name Zoneset-Server-NetApp vsan 1007
    N9K-A(config)# zone commit vsan 1007
    Commit operation initiated. Check zone status
    ```
> :bulb: You can append the **force** keyword after the **commit** command to override a user lock. It is descouraged, however, ebcause this action will erase all configuration written by the user acquiring the lock.
7.  On both Nexus and MDS, verify the configured and active zone for your VSAN:  
    ```
    N9K-A# show zone vsan 1007
    zone name Server-NetApp vsan 1007
      device-alias Server
      device-alias test
    ```
    ```
    N9K-A# show zone active vsan 1007
    zone name Server-NetApp vsan 1007
    * fcid 0x1c0000 [device-alias Server]
      device-alias test
    ```
    ```
    MDS# show zone vsan 1007
    zone name Server-NetApp vsan 1007
      device-alias Server
      device-alias test
    ```
    ```
    MDS# show zone active vsan 1007
    zone name Server-NetApp vsan 1007
    * fcid 0x1c0000 [device-alias Server]
      device-alias test
    ```

## Resolve Zoning Merger Failures
Shutting down a SAN port channel will also operatively bring down member interfaces. Having disconnected the Nexus from the MDS, you will make a change to an existing member of your zone. After reconnecting the NExus with the MDS, the two switches will have different active databases and the inconsistency will create a merger conflict.
```
N9K-A(config)# interface san-port-channel 10
N9K-A(config-if)# shutdown
```
Configure a device alias for an unused pWWN
```
N9K-A(config)# device-alias database
N9K-A(config)# device-alias name Conflict pwwn 20:00:00:25:b5:00:00:02
N9K-A(config)# device-alias commit
N9K-A(config)# show device-alias database
device-alias name test pwwn 20:00:00:25:b5:00:00:01
device-alias name Server pwwn SRV_PWWN
device-alias name Conflict pwwn 20:00:00:25:b5:00:00:02

Total number of entries = 3
```
add the device alias to your zone
```
N9K-A(config)# zone name Server-NetApp vsan 1028
N9K-A(config)# member device-alias Conflict
Enhanced zone session has been created. Please 'commit' the changes when done.
```
Activate your zoneset and commit the zone changes for the VSAN
```
N9K-A(config)# zoneset activate name Zoneset-Server-NetApp vsan 1007
N9K-A(config)# zone commit vsan 1007
Commit operation initiated. Check zone status
```
Bring the SAN port back up
```
N9K-A(config)# interface san-port-channel 10
N9K-A(config)# no shutdown
```
Examine the trunking status of the SAN port channel
```
N9K-A# show interface san-port-channel 10
san-port-channel10 is trunking (Not all VSANs UP on the trunk)
    Hardware is Fibre Channel
    Port WWN is 24:0a:ac:4a:67:de:69:e0
    Admin port mode is E, trunk mode is on
    snmp link state traps are enabled
    Port mode is TE
    Port vsan is 1007
    Speed is 16 Gbps
    Trunk vsans (admin allowed and active) (1,1028)
    Trunk vsans (up)                       (1)
    Trunk vsans (isolated)                 (1028)   <-------------------
    Trunk vsans (initializing)             ()
<... output omitted ...>
```
Resolve the merger failure by importing the zoneset from MDS
```
N9K-A# show interface san-port-channel 10
san-port-channel10 is trunking
    Hardware is Fibre Channel
    Port WWN is 24:0a:ac:4a:67:de:69:e0
    Admin port mode is E, trunk mode is on
    snmp link state traps are enabled
    Port mode is TE
    Port vsan is 1007
    Speed is 16 Gbps
    Trunk vsans (admin allowed and active) (1, 1028)
    Trunk vsans (up)                       (1)
    Trunk vsans (isolated)                 ()
    Trunk vsans (initializing)             (1028)   <-------------------
<... output omitted ...>
```
```
N9K-A# show interface san-port-channel 10
san-port-channel10 is trunking
    Hardware is Fibre Channel
    Port WWN is 24:0a:ac:4a:67:de:69:e0
    Admin port mode is E, trunk mode is on
    snmp link state traps are enabled
    Port mode is TE
    Port vsan is 1007
    Speed is 16 Gbps
    Trunk vsans (admin allowed and active) (1,1028)
    Trunk vsans (up)                       (1,1028)   <-------------------
    Trunk vsans (isolated)                 ()
    Trunk vsans (initializing)             ()
<... output omitted ...>
```
Investigate the active zone set for your VSAN
```
N9K-A# show zone active vsan 1028
zone name Server-NetApp vsan 1028
* fcid 0x1c0000 [device-alias Server]
  device-alias test
```
*The device alias Conflict is gone because the import has overwritten the previous zone set on the Nexus Switch*
On the MDS, verify the interface which connects the MDS to the core MDS:
```
MDS# show interface fc1/3
fc1/3 is down (Isolation due to zone merge failure)   <-------------------
    Port description is Connects to MDS-CORE
    Hardware is Fibre Channel, SFP is short wave laser w/o OFC (SN)
    Port WWN is 20:03:00:3a:9c:54:3d:f0
    Admin port mode is E, trunk mode is auto
    snmp link state traps are enabled
    Port vsan is 1028
   <... output omitted ...>
```
On the MDS, resolve the merger failure by importing the zone set from the core MDS:  
```MDS# zoneset import interface fc1/3 vsan 1028```
> :bulb: wait about 30 seconds and verify the active zonesets
```
MDS# show zoneset active vsan 1028
zoneset name Zoneset-Host-NetApp vsan 1028
  zone name Host-NetApp vsan 1028
  * fcid 0x1c0000 [pwwn 20:00:00:25:b5:00:07:01] [Server]
    pwwn 20:00:00:25:b5:10:70:10
  * fcid 0xad3ea6 [pwwn 20:03:00:a0:98:d5:6c:36]
  * fcid 0xad40a6 [pwwn 20:05:00:a0:98:d5:6c:36]

  zone name IVRZ_EG00-Pod07 vsan 1028
  * fcid 0x1c0000 [pwwn 20:00:00:25:b5:00:07:01] [Server]
    pwwn 20:00:00:25:b5:10:70:10
  * fcid 0xad3ea6 [pwwn 20:03:00:a0:98:d5:6c:36]
  * fcid 0xad40a6 [pwwn 20:05:00:a0:98:d5:6c:36]
```

### Smart Zoning 

1.  Enable Cisco Smart Zoning: `zone smart-zoning enable vsan <vsan>  `
2.  Convert zones automatically to Smart Zoning:  `zone convert smart-zoning vsan <vsan>` 
3.  Activate a zone set:                          `zoneset activate name <zoneset_name> vsan <vsan>`

When Smart Zoning is functional, add the keywords **init**, **target**, and **both** to the memeber entries.
Cisco Smart Zoning programs only TCAMs with entries that connet the initiator and trhe target.  
**member [device-alias | pwwn | fcid] *name* [init | target | both]**  
To investigate if Cisco Smart Zoning is enabled for a particular VSAN, use the **show zone status vsan *vsan*** command.  

It is recommended that you use as many zones as there are host but adapters (HBAs) that are communitating with storage. For example, if there are two hosts, each with two HBAs, which are communitating with three storage devices, it is recommended taht you use four zones.  

To simplify management, you should use device aliasses whereever possible. It is easier to identify devices with aliasses than with WWNs.  

Leave the default policy as **deny** so that devices cannot inadvertently access each other when placed in the default zone. Changing the default policy **allow** is a bad practice.  


