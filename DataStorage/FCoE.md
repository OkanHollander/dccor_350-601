# Basic FCoE Configuration Cisco Nexus 5000 Series

## FCoE configuration on Cisco Nexus 5000 series requires these steps:
### 1. Enable FCoE and LLDP features
`switch(config)# feature fcoe`  
`switch(config)# feature lldp`
### 2. Create the VLAN and map the VLAN to VSAN that is designated as FCoE.
`switch(config)# vlan 200`  
`switch(config-if)# fcoe vsan 200`  
### 3. Configure the VLANs and STP edge trunk on a physical Ethernet interface:
`switch(config)#  interface ethernet 1/1`  
`switch(config-if)# shutdown`  
`switch(config-if)# switchport mode trunk`  
`switch(config-if)# switchport trunk native vlan 56`  
`switch(config-if)# switchport trunk allowed vlan 56, 200`  
`switch(config-if)# spanning-tree port type edge`  
`switch(config-if)# no shutdown`  
### 4. Create a virtual Fibre Channel interface and bind it to the physical Ethernet interface.
`switch(config)# interface vfc 200`  
`switch(config-if)# bind interface ethernet 1/1`  
### 5. Associate the virtual Fibre Channel interface to the VSAN.
`switch(config)# vsan database`  
`switch(config-vsan)# vsan 200 interface vfc 200`  

# Basic FCoE Configuration Cisco Nexus 7000 Series

Two-part configuration  
1.  In the default VDC, configure a dedicated storage VDC.  
2.  Switch to the newly created storage VDC and configure it for FCoE as you would do on the Cisco Nexus 5000 Series switch.  

## FCoE configuration steps on a Cisco Nexus 7000 within the default VDC are:  
### 1. License each module that you will use for FCoE.
`switch(config)# license fcoe module 2`  
### 2. Install the FCoE feature set in the default VDC.
`switch(config)# install feature-set fcoe`  
### 3. Enable FCoE QoS
`switch(config)# system qos`  
`switch(config-sys-qos)# service-policy type network-qos default-nq-7e-policy`  
### 4. Configure FCoE interfaces for trunking.
`switch(config)# interface eth2/1-10`  
`switch(config-if)# switchport mode trunk`  
`switch(config-if)# spanning-tree port type edge trunk`  
### 5. Configure the storage VDC and allocate interfaces and VLANs used for FCoE.
`switch(config)# vdc FCoE_VDC type storage`  
`switch(config-vdc)# allocate interface ethernet 2/15`  
`switch(config-vdc)# allocate shared interface ethernet 2/1-5`  
`switch(config-vdc)# allocate fcoe-vlan-range 10-20 from vdc DEFAULT`  

# FcoE Over FEX
## FcoE over FEX configuration steps that differ from setup without FEX are:
### 1. Enable FCoE and LLDP feature, and FEX for FCoE.
`switch(config)# feature fcoe`  
`switch(config)# feature llsp`  
`switch(config)# fex 100`  
`switch(config-fex)# fcoe`  
### 2. Configure switch physical interface as FEX fabric and assiciate it with FEX.
`switch(config)# interface ethernet 1/1`  
`switch(config-if)# switchport mode fex-fabric`  
`switch(config-if)# fex associate 100`  

`switch(config)# interface Ethernet 100/1/1`  
`switch(config-if)# switchport mode trunk`  
`switch(config-if)# switchport trunk native vlan 56`  
`switch(config-if)# switchport trunk allowed vlan 56,200`  
`switch(config-if)# spanning-tree type port edge trunk`  
### 3. Treat the FEX interface the same as an interface on a Cisco Nexus 5000 that has a directly connected server.
`switch(config)# vlan 200`  
`switch(config-vlan)# fcoe vsan 200`  

`switch(config)# interface vfc200`  
`switch(config-if)# bind interface ethernet 100/1/1`  
`switch(config-if)# no shutdown`  

`switch(config)# vsan database`  
`switch(config-vsan)# vsan 200 interface vfc 200`  

# FcoE configuration on Cisco Nexus 9000 Series requires these steps:
## 1. Enable LLDP feature and install and enable FCoE feature-set
`switch(config)# feature lldp`  
`switch(config)# install feature-set fcoe`  
`switch(config)# feature-set fcoe`  
## 2. Perform TCAM carving. When you enter the feature-set fcoe command, system will show you the following hint:
```
Please configure the following for fcoe to be fully functional:
- hardware access-list tcam region ing-racl TCAM size
- hardware access-list tcam region ing-ifacl TCAM size
- hardware access-list tcam region ing-redirect TCAM size
```
If you want to use a non-default feature for Cisco Nexus 9000 Series switches, you must manually carve out the TCAM space for the features:  
`switch(config)# hardware access-list tcam region ing-racl 1536`  
`switch(config)# hardware access-list tcam region ing-ifacl 256`  
`switch(config)# hardware access-list tcam region ing-redirect 256`  
## 3. Verify the configured TCAM region size (Optional):  
`switch(config)# show hardware access-list tcam region`  
## 4. Save the configuration and reload the switch.  
`switch(config)# copy running-config startup-config`  
`switch(config)# reload`  
## 5. Create the VSAN, which will transfer the SAN traffic.

`switch(config)# vsan database`  
`switch(config-vsan-db)# vsan 200`  
## 6. Create the VLAN and map the VLAN to VSAN that is designated as FCoE.

`switch(config)# vlan 200`  
`switch(config-vlan)# fcoe vsan 200`  
## 7. Configure the VLANs and STP edge trunk on a physical Ethernet interface  
`switch(config)# interface Ethernet 1/1`  
`switch(config-if)# shutdown`  
`switch(config-if)# switchport mode trunk`  
`switch(config-if)# switchport trunk native vlan 56`  
`switch(config-if)# switchport trunk allowed vlan 56, 200`  
`switch(config-if)# spanning-tree port type edge trunk`  
`switch(config-if)# mtu 9216`  
`switch(config-if)# service-policy type qos input default-fcoe-in-policy`  
`switch(config-if)# no shutdown`  

> **Note**
***The Ethernet or port-channel interface must be configured with MTU 9216 (or the maximum available MTU size) for supporting FCoE.***  

## 7. Create a virtual Fibre Channel interface and bind it to the physical Ethernet interface.

`switch(config)# interface vfc 200`  
`switch(config-if)# bind interface Ethernet 1/1`  
## 8. Associate the virtual Fibre Channel interface to the VSAN.

`switch(config)# vsan database`  
`switch(config-vsan)# vsan 200 interface vfc 200`  