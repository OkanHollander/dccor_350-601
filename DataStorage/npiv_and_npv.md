# NPIV AND NPV

## NPV
### NPV initialization steps
1.  NP Port becomes operational.  
2.  Edge switch logs itself in to the code switch using FLOGI.  
    - Uses the pWWN of the NP Port.  
3.  Edge switch registers itself with the name of the core switch.
    - Uses symbolic port name of the NP Port and IP address of the edge switch.  
4.  NPV edge switch converts subsequent FLOGIs from servers to FDISCs.  
![alt text](images/NPV_Edge_Switch.png)

### F and TF Port Channels on NPV Code Switch
An F Port channel is a logical interface that combines a set of fabric ports that are connected to the same Fabric Channel Mode. The F port channel operates as one link between the F Port and the NP Port. An F Port Channel supports the same bandwidth utilization and availability as an E Port channel. The F Port channel is used mainly between a NPV code and NPV switch, to provide optimal bandwidth utilization and transparent failover between the uplkinks of a VSAN.
![alt text](images/F_and_TF_Port_Channels.png)
A TF Port channel is a trunking F Port on a NPV code switch carrying multiple VSAN traffic to and from an NP Port.
> :bulb: **Note**: On the NPV core switch, you will not be allowed to configure TF Port cahnneling unless you first enable the **fport-channel-trunk** feature.

## Migrate San A to the NPV Design
The following high-level subtasks are discussed to migrate a Nexus Switch (N9K-A) as an NPV switch:
1.  Enable NPV on N9K-A. This action will make the switch reload and erase most of the configuration.
2.  Reconfigure the N9K-A and configure interfaces towards MDS to function as Np (Node Proxy) Ports.
3.  Configure interfaces on MDS towards N9K-A to function as F Ports.
4.  Investigate how the behavior on N9K-A and MDS changed with the conversion.  

## Enable NPV on N9K-A.
1.  On N9K-A, enable the NPV-FC-E feature set.
    ```
    install feature-set fcoe-npv
    ```
    Example:
    ```
    N9K-A(config)# install feature-set fcoe-npv
    fcoe-npv feature-set cannot be installed since fcoe feature-set is already installed/enabled(0x40aa002c)
    ```
2.  On N9K-A, try to disable the FCoE feature set.  
    ```
    slot 1
    port-45-48 type ethernet
    ```
    Example:
    ```
    N9K-A(config)# slot 1
    N9K-A(config-slot)# port 45-48 type ethernet
    Port type is changed. ACTION REQUIRED: Please save configurations and reload the switch
    ```
3.  After login, disable and uninstall the FCoE feature set. Then save the configuration and reload the switch again.
    ```
    no feature-set fcoe
    no install feature-set fcoe
    copy running-config startup-config
    reload
    ```
    Example:
    ```
    N9K-A(config)# no feature-set fcoe
    Feature-set Operation may take up to 30 minutes depending on the size of configuration.

    Disabling feature-set. ACTION REQUIRED: Copy the running config to startup and reload the box for the disable to take effect correctly
    N9K-A(config)# no install feature-set fcoe
    N9K-A(config)# copy running-config startup-config
    [########################################] 100%
    Copy complete, now saving to disk (please wait)...
    Copy complete.
    N9K-A(config)# reload
    This command will reboot the system. (y/n)?  [n] y
    ```
4.  After login, install and enable the FCoE-NPV feature set. This will install both Fibre Channel and FCoE-NPV feature sets.
    ```
    install feature-set fcoe-npv
    feature-set fcoe-npv
    ```
    Example:
    ```
    N9K-A(config)# install feature-set fcoe-npv
    N9K-A(config)# feature-set fcoe-npv
    ```
5.  On N9K-A, configure ports uplink to the parent switch as Fibre Channel ports.  
    ```
    slot 1
    port 45-48 type fc
    copy running-config startup-config
    reload
    ```
    Example:
    ```
    N9K-A(config)# slot 1
    N9K-A(config-slot)# port 45-48 type fc
    Port type is changed. Please reload the switch
    N9K-A(config-slot)# copy running-config startup-config
    [########################################] 100%
    Copy complete, now saving to disk (please wait)...
    N9K-A(config-slot)# reload
    WARNING: This command will reboot the system
    Do you want to continue? (y/n) [n] y
    <... output omitted ...>
    ```
6.  On N9K-A, configure everything as it was before the conversion to NPV, except for the switch port mode on the uplink to MDS; set the switch port mode of that interface to NP.
    A.  Configure the Fibre Channel interfaces.
    ```
    N9K-A(config)# interface fc1/47-48
    N9K-A(config-if)# channel-group 10
    fc1/47 fc1/48 added to port-channel 10 and disabled
    please do the same operation on the switch at the other end of the port-channel,
    then do "no shutdown" at both ends to bring it up
    N9K-A(config-if)# no shutdown
    ```
    B.  When it comes to the switch port mode configuration on the upstream interface (to the parent switch, MDS), you should configure the NP mode.
    ```
    N9K-A(config-if)# interface san-port-channel 10
    N9K-A(config-if)# switchport mode ?
      E     E port mode
      F     F port mode
      NP    NP port mode for N-Port Virtualizer (NPV) only
      auto  Autosense mode
    N9K-A(config-if)# switchport mode np
    N9K-A(config-if)# channel mode active
    ```
    C.  Map your vsan to vlan 1000. Create a vDC interface and bind it to the Ethernet1/5 interface.
    ```
    N9K-A(config)# vlan 1000
    N9K-A(config-vlan)# fcoe vsan POD_VSAN
    N9K-A(config-vlan)# interface vfc1000
    N9K-A(config-if)# bind interface Ethernet1/5
    N9K-A(config-if)# switchport trunk allowed vsan POD_VSAN
    Warning: This command will remove all VSANs currently being trunked and trunk only the specified VSANs.
    Do you want to continue? (y/n) [n] y
    N9K-A(config-if)# no shutdown

    N9K-A(config-if)# vsan database
    N9K-A(config-vsan-db)# vsan POD_VSAN
    N9K-A(config-vsan-db)# vsan POD_VSAN interface san-port-channel 10
    N9K-A(config-vsan-db)# vsan POD_VSAN interface vfc1000
    ```
    You will notice that some of the commands that were present before the conversion to NPV will not work now. NPV switch does not run all storage services and does not consume a domain ID.
7.  To enable trunking and for the FLOGI from NP uplink of Cisco Nexus 9000 Switches to be succesful on the core switch (MDS), teh core should be configured with the OUI of a Cisco Nexus 9000 Sesies Switch. Check the OUI of the N9K Switch.
    ```
    N9K-A(config-if)# show wwn switch 
    Switch WWN is 20:00:ac:4a:67:bc:d4:40
    ```
8.  On the MDS, check if the OUI is already registered. If the OUI is not registered with eh MDS, configure it manually
    ```
    MDS# show wwn oui | i ac4a67
    MDS# configure
    MDS(config)# wwn oui 0xac4a67
    ```
9.  On the MDS, configure interfaces fc1/1 and fc1/2 (which connect to your N9K-A) as F Ports.  
    Because interfaces fc1/1 adn fc1/2 are already bundled into port channel 10, you should not perform the configuration on the interface level but on port cahnnel level. Even more, the switch will not even allow you to perform the configuration on the interface level. Shut down the port and then re-enable it.
    ```
    MDS(config)# interface port-channel 10
    MDS(config-if)# switchport mode f
    MDS(config-if)# channel mode active
    MDS(config-if)# shutdown
    MDS(config-if)# no shutdown
    ```
10. On N9K-A, bounce the san-port-channel 10
    ```
    N9K-A(config)# interface san-port-channel 10
    N9K-A(config-if)# shutdown
    N9K-A(config-if)# no shutdown
    ```
11. On N9K-A and MDS, verify that the port channel 10 is operational
    ```
    MDS# show interface port-channel 10
    port-channel 10 is up
    <... output omitted ...>
        Port mode is F
        Port vsan is 1018
    <... output omitted ...>
    ```
    You should see that the port channel is **up**, the mode is **F**, and VSAN is 1018
    ```
    N9K-A# show interface san-port-channel 10
    san-port-channel 10 is up
    <... output omitted ...>
        Port mode is NP
        Port vsan is 1018
    <... output omitted ...>
    ```
    You should see that port channel is **up**, the mode is **NP**, adn VSAN is 1018
12. On N9K-A, investigate the status of NPV
    ```
    N9K-A# show npv status

    npiv is disabled

    disruptive load balancing is disabled

    External Interfaces:
    ====================
      Interface: san-port-channel 10, State: Up
            VSAN:   1018, State: Up, FCID: 0xa50000

      Number of External Interfaces: 1

    Server Interfaces:
    ==================
      Interface: vfc1000, VSAN:   POD_VSAN, State: Up

      Number of Server Interfaces: 1
    ```
    You should see one external (SAN port channel 10) and one server (vFC 1000) interface listed and operational. External interfaces are northbound interfaces (connected to MDS) and internal interfaces are southbound interfaces (connected to hosts).
13. On N9K-A, investigate the FLOGI database, and the FCNS database.

    You will see that N9K-A does not know any of the regular SAN commands, now that you turned it into an NPV switch. The NPV switch will just proxy all SAN information to the parent switch.

    Cisco NPV edge switches are essentially transparent to the fabric, and most Fibre Channel services of the switches are disabled. The switches do not have domain IDs, so they do not appear in the FSPF routing table. From the perspective of SAN, the target or initiator that is connected to the NPV edge switch, will appear as directly connected to the NPV core switch.
    ```
    N9K-A# show flogi database
                             ^
    % Invalid command at '^' marker.
    N9K-A# show fcns database
                              ^
    % Invalid command at '^' marker.
    ```
14. On MDS, investigate the FLOGI database, the FCNS database, and active zoning for VSAN 1018.
    ```
    MDS# show flogi database
    ---------------------------------------------------------------------------
    INTERFACE        VSAN      FCID             PORT NAME               NODE NAME
    ---------------------------------------------------------------------------
    port-channel10   POD_VSAN  0xa50000  24:0a:ac:4a:67:bc:d4:40 23:ee:ac:4a:67:bc:d4:41
    port-channel10   POD_VSAN  0xa50001         SRV_WWPN                SRV_WWNN
    Total number of flogi = 2.

    MDS# show fcns database

    VSAN POD_VSAN:
    --------------------------------------------------------------------------
    FCID        TYPE  PWWN                    (VENDOR)        FC4-TYPE:FEATURE
    --------------------------------------------------------------------------
    0x010100    N     24:0a:ac:4a:67:bc:d4:40 (Cisco)         229
    0x010102    N     SRV_WWPN                (Cisco)         scsi-fcp:init fc-gs
    <... output omitted ...>
    ```
15. On N9K-A, investigate the NPV FLOGI table.

    So while there is no show flogi database command on a NPV-enabled switch, you can use the show npv flogi-table command to see devices that logged in to the parent switch through the NPV-enabled switch.
    ```
    N9K-A# show npv flogi-table
    --------------------------------------------------------------------------------
    SERVER                                PORT NAME                EXTERNAL
    INTERFACE       VSAN     FCID        (NODE NAME)               INTERFACE
    --------------------------------------------------------------------------------
    vfc1000         POD_VSAN 0xa50001     SRV_WWPN                 San-po10
                                     (SRV_WWNN)

    Total number of flogi = 1.
    ```
16. Save configurations on N9K-A and MDS switches.  
    `N9K-A# copy running-config startup-config`  
    `MDS# copy running-config startup-config`  
