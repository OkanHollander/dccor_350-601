# Fibre Channel Over IP
FCIP is a tunneling protocol that you would use to connect geographically distributed Fibre Chennel storage area networks transparently over an IP network. FCIP is an industry-standard protocol.
> :bulb: When you use FCIP, you are essentially carrying Small Computer Systems Interface (SCSI) traffic over Fibre Channel Protocol (FCP) over TCP over IP. Think of FCIP as carrying IPv6 traffic over IPv4 Generic Routing Encapsulation (GRE) tunnel.

FCIP virtual expansion ports (VE Ports) behave exactly like standard Fibre Channel E Ports, except that the transport is FCIP instead of Fibre Channel.  

## FCIP Configuration
Before you start your FCIP configuration, you should make sure that you have IP connectivity between the two FCIP gateways. Consider changing the maximum transmission unit (MTU) size. If the entire data path support jumbo frames, specify an MTU of 2300. This configuration should achieve double the throughput when IP Security (IPsec) is enabled. If compression is enabled, changing the MTU will not improve performance with or without IPsec.  
The example in the following figure shows two Cisco MDS switches connected though FCIP, along with the configuration on the side of SW1. The configuration on SW02 mirrors the configuration of SW1; like a GRE configuration would (with swapped source and destination).
1.  Enable the FCIP feature.  
    ```
    feature fcip
    ```
2.  Create an FCIP profile.  
    ```
    fcip profile 10
      ip address 10.100.1.25
    ```
3.  Create an FCIP link.
    ```
    interface fcip 10
      use-profile 10
      peer-info ipaddr 10.1.1.1
      no shutdown
    ```
![alt text](images/fcip_config.png)  
To begin configuring the FCIP feature, you must explicitly enable FCIP on the required switches in the fabric. By default, this feature is disabled on all switches in the Cisco MDS 9000 Family. The configuration and verification commands for the FCIP feature are only available when FCIP is enabled on a switch. When you disable this feature, all related configurations are automatically discarded.  

Path maximum transmission unit (PTMU) is the minimum MTU on the IP network between the two endpoints of the FCIP link. PTMU discovery is a mechanism by which TCP learns of the PTMU dynamically and adjusts the maximum TCP segment accordingly. By default, PMTU discovery is enabled on all switches with a timeout of 3600 seconds.  

## FCIP Verification
To display all configured FCIP profiles from the CLI, use the **show fcip profile** command from the EXEC mode. The output shows the profile Ids, their associated IP addresses, and their TCP ports, as indicated in this output from SW1 in this example topology.  
In addition, you can go deeper into the configuration and display additional TCP parameters for each FCIP profile. To do so, issue the **show fcip profile id** command, where **id** is a particular configured profile identifier. The example output shows the current values for the TCP parameters of that profile.  
```
SW1# show fcip profile
--------------------------------------------------
ProfileId Ipaddr TcpPort
--------------------------------------------------
1010.100.1.253225
SW1# show fcip profile 10
FCIP Profile 10
   Internet Address is 10.100.1.25 (interface GigabitEthernet3/1) 
   Tunnels Using this Profile: fcip1 
   Listen Port is 3225 
   TCP parameters
      SACK is enabled
      PMTU discovery is enabled, reset timeout is 3600 sec
      Keep alive is 60 secMinimum retransmission timeout is 200 ms
      <... output omitted ...>
```
