## Aliassing

### Configure Device Aliassing
Check alias status: `device-alias status`  

Change distributing modes and commit the changes:  
`device-alias mode enhanced`  
`device-alias commit`  
Configure alias for pwwn:  
```
configure
device-alias database
device-alias name <name> pwwn <PRV_PWWN>
device-alias commit
```  